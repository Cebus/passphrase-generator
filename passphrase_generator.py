import os                  # Getting relative file path, pausing program window
import secrets             # Cryptographically secure random number generator
import math                # Calculating entropy
# from ctypes import windll  # Clear windows clipboard
# try:
    # from Tkinter import Tk # python 2
# except ImportError:
    # from tkinter import Tk # python 3

# Constants and variables:

# Number of words in pw
pw_length = 8
# Relative file path to file of words from current
filename = 'corncob_lowercase.txt'
# Get current directory path
dirname = os.path.dirname(__file__)
# Use "," to separate path instead of "\" 
filename = os.path.join(dirname, filename)
# Final password for user
pw = ""

print("\n")

f = open(filename, 'r')
# Store words as elements in a list: words
words = f.read().splitlines() 
f.close()
words_len = len(words)
word_entropy = math.log(words_len,2)
# Reduce to 2 decimal places
word_entropy = float("{0:.2f}".format(word_entropy))

# Generate passwords from random words
for i in range(pw_length):
	pw += secrets.choice(words) + " "

print("===stats===")
print("number of possible words:",words_len)
print("entropy per word:",word_entropy)
print("total entropy of password:",word_entropy*pw_length)
print("\n===password===")
print(pw,"\n")

# # Put password in clipboard temporarily
# # Time before clipboard is destroyed in ms
#destroy_time = 5000
# r = Tk()
# r.withdraw()
# r.clipboard_clear()
# r.clipboard_append(pw)
# r.after(destroy_time, r.destroy)
# r.mainloop()

# # Clear clipboard

# if windll.user32.OpenClipboard(None):
	# windll.user32.EmptyClipboard()
	# windll.user32.CloseClipboard()
